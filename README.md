# Feature Modeling Principles

With this work we provide the first carefully synthesized set of industry-relevant modeling principles for feature models, almost 30 years after feature models were introduced:

Damir Nesic, Jacob Krueger, Stefan Stanciulescu, Thorsten Berger, “Principles of Feature Modeling,” in 27th ACM SIGSOFT International Symposium on the Foundations of Software Engineering (FSE), 2019
Preprint: http://www.cse.chalmers.se/~bergert/paper/2019-fse-fm_principles.pdf

### Survey about the Principles
In case you have time, we would be excited if you could help us further refining and extending the principles by filling in the following survey, or forwarding to your contacts who have experience with feature modeling. It's a bit lengthy, though, and probably takes 20-30min:
https://docs.google.com/forms/d/e/1FAIpQLSeA0txEf7GOGnfiegqrhHc04u89_kDdKSK8MhuWB_qk8wXTNw/viewform



### Content of this Repository

File 'Identified literature\StartingPapers.bib' contains the bibliographic entries of all papers discovered during the manual analysis of the last five instances of the selected conferences.

File 'Identified literature\SnowballPapers.bib' contains the bibliographic entries of all papers discovered by applying the snowballing method on the initial 6 papers

File 'Identified literature\ACM search results' contains the bibliographic entries of all the results for running the search string (+Experience Report +(Variability OR Feature) +(Specification OR Model)).  

Folder 'Identified literature\snowball' contains the bibliography file of each of the 31 papers that were used as the basis for the identification of principles.

